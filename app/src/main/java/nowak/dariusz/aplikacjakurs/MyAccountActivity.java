package nowak.dariusz.aplikacjakurs;

import android.databinding.DataBindingUtil;
import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.Calendar;

import nowak.dariusz.aplikacjakurs.databinding.ActivityMyAccountBinding;

import static android.R.attr.value;

public class MyAccountActivity extends AppCompatActivity {

    private String firstName;
    private String surName;

    TextView textView;
    EditText editText;
    Button buttonYer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityMyAccountBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_my_account);

//        firstName = getIntent().getStringExtra("firstName");
//        surName = getIntent().getStringExtra("surName");

        final User user = (User) getIntent().getSerializableExtra("USER");
        binding.setUser(user);

        textView = (TextView) findViewById(R.id.setAge);
        editText = (EditText) findViewById(R.id.editText);
        buttonYer = (Button) findViewById(R.id.buttonYer);
        final Calendar calendar = Calendar.getInstance();

        final int[] yerOfBirth = {0};


        buttonYer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("kru", "klikam");

                if(!editText.getText().toString().equals("")) {
                    yerOfBirth[0] = Integer.parseInt(editText.getText().toString());
                    int actuallyYer = calendar.get(Calendar.YEAR);
                    final String value = actuallyYer - yerOfBirth[0] +"";
                    textView.setText(value);
                }


            }
        });


    }


    private static void calculateAge(TextView textView, EditText editText){

            Calendar calendar = Calendar.getInstance();
            int yerOfBirth = Integer.valueOf(editText.getText().toString());
            int actuallyYer = calendar.get(Calendar.YEAR);
            textView.setText(String.valueOf(actuallyYer - yerOfBirth));


    }
}
