package nowak.dariusz.aplikacjakurs;

import java.io.Serializable;

/**
 * Created by RENT on 2017-06-06.
 */

public class User implements Serializable{

    private final String firstName;
    private final String lastName;
    private final boolean spam;

    public User(String firstName, String lastName, boolean spam) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.spam = spam;

    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public boolean getSpam() {
        return spam;
    }

}
