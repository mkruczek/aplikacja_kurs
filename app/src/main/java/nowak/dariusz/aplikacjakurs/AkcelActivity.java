package nowak.dariusz.aplikacjakurs;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.CountDownTimer;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.EditText;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AkcelActivity extends AppCompatActivity implements SensorEventListener {

    @BindView(R.id.x)
    EditText xFile;
    @BindView(R.id.y)
    EditText yFile;
    @BindView(R.id.z)
    EditText zFile;

    float x;
    float y;
    float z;

    private SensorManager sensorManager;
    private Sensor mAccelerometr;
    private Sensor mTemperature;

    public final String TAG = "kru";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_akcel);

        ButterKnife.bind(this);

        sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        mAccelerometr = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        mTemperature = sensorManager.getDefaultSensor(Sensor.TYPE_AMBIENT_TEMPERATURE);

        new CountDownTimer(30000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {

                long counter = millisUntilFinished/1000;
                Log.d(TAG, String.valueOf(counter));
//              test1();
                counter--;
            }

            @Override
            public void onFinish() {
                Log.d(TAG, "KONIEC");
            }
        }.start();

    }

    @Override
    public void onResume() {
        super.onResume();
        sensorManager.registerListener(this, mAccelerometr, SensorManager.SENSOR_DELAY_NORMAL);
        sensorManager.registerListener(this, mTemperature, SensorManager.SENSOR_DELAY_NORMAL);
    }

    float abc;

    @Override
    public void onSensorChanged(final SensorEvent event) {
        if (mAccelerometr.getType() == Sensor.TYPE_ACCELEROMETER) {
            x = event.values[0];
            y = event.values[1];
            z = event.values[2];

            xFile.setText(Float.toString(x), TextView.BufferType.EDITABLE);
            yFile.setText(Float.toString(y), TextView.BufferType.EDITABLE);
            zFile.setText(Float.toString(z), TextView.BufferType.EDITABLE);

        }

//        if (event.sensor.getType() == Sensor.TYPE_AMBIENT_TEMPERATURE){
//            final float abc = event.values[0];
//            xFile.postDelayed(new Runnable() {
//                @Override
//                public void run() {
//                    Log.d(TAG, String.valueOf(abc));
//                }
//            }, 1000);
//        }

//        xFile.postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                if (event.sensor.getType() == Sensor.TYPE_AMBIENT_TEMPERATURE) {
//                    final float abc = event.values[0];
//                    Log.d(TAG, String.valueOf(abc));
//                }
//            }
//        }, 1000);

        if (event.sensor.getType() == Sensor.TYPE_AMBIENT_TEMPERATURE) {
            abc = event.values[0];

        }

    }


    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    public void test1() {

        Log.d(TAG, String.valueOf(abc));

    }

}
