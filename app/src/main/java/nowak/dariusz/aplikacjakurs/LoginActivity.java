package nowak.dariusz.aplikacjakurs;

import android.content.Context;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.Toast;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.OnClick;


public class LoginActivity extends AppCompatActivity {

    TextInputLayout usernameWrapper;
    TextInputLayout passwordWrapper;
    Button btnLogin;
    CheckBox checkBox;

    private static final String EMAIL_PATTERN =
            "^[a-zA-Z0-9#_~!$&'()*+,;=:.\"(),:;<>@\\[\\]\\\\]+@[a-zA-Z0-9-]+(\\.[a-zA-Z0-9-]+)*$";
    private Pattern pattern = Pattern.compile(EMAIL_PATTERN);
    private Matcher matcher;

    public boolean validateEmail(String email) {
        matcher = pattern.matcher(email);
        return matcher.matches();
    }

    public boolean validatePassword(String password) {
        return password.length() >= 10;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        usernameWrapper = (TextInputLayout) findViewById(R.id.usernameWrapper);
        passwordWrapper = (TextInputLayout) findViewById(R.id.passwordWrapper);
        btnLogin = (Button) findViewById(R.id.btnLogin);
        checkBox = (CheckBox) findViewById(R.id.checkBox);

        usernameWrapper.setHint("Username");
        passwordWrapper.setHint("Password");

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String username = usernameWrapper.getEditText().getText().toString();
                String password = passwordWrapper.getEditText().getText().toString();

                eMailValidation(username);
                passwordValidation(password);

                if (validateEmail(username) && validatePassword(password)) {
                    login();
                }
            }

            private void passwordValidation(String password) {
                if (!validatePassword(password)) {
                    passwordWrapper.setError("Password must be at least 10 characters length.");
                } else {
                    passwordWrapper.setErrorEnabled(false);
                }
            }

            private void eMailValidation(String username) {
                if (!validateEmail(username)) {
                    usernameWrapper.setError("Not a valid email address!");
                } else {
                    usernameWrapper.setErrorEnabled(false);
                }
            }

        });


        checkBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkBox.isChecked()) {
                    passwordWrapper.getEditText().setTransformationMethod(null);
                } else {
                    passwordWrapper.getEditText().setTransformationMethod(new PasswordTransformationMethod());
                }
            }
        });
    }

    private void login() {
        //TODO: create login implementation
        Context context = getApplicationContext();
        CharSequence text = "ZALOGOWANY";
        int duration = Toast.LENGTH_SHORT;

        Toast toast = Toast.makeText(context,text,duration);
        toast.show();
    }



}
