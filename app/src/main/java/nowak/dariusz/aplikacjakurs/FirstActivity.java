package nowak.dariusz.aplikacjakurs;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;


public class FirstActivity extends AppCompatActivity {

    // nasz TAG za pomoca ktorego bedziemy mogli filtrowac wiadomosci w Android Monitor
    public static final String TAG = "DarekApp";

    String wpisanyTekst;

    // ponizej zainicjowalismy kontrolke aby byla dostepna poza onCreate
    EditText wpisanyAdresUrl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_first);
        Log.d(TAG, "FirstActivity: onCreate()");

        ImageView imageView = (ImageView) findViewById(R.id.imageView);
        String imageUri = "http://";

        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(this).build();
        ImageLoader.getInstance().init(config);

        ImageLoader imageLoader = ImageLoader.getInstance();
        imageLoader.displayImage(imageUri, imageView);
//        imageLoader.loadImage(imageUri, new SimpleImageLoadingListener() {
//            @Override
//            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
//                // Do whatever you want with Bitmap
//            }
//        });

        // aktywnosc jest tworzona

        // odczytujemy kontrolki z naszego pliku xml korzystajac z metody findViewById i odnoszac sie do Id zapisanego w xml
        // po lewej jest nasza nazwa wlasna ktora poslugujemy sie w obrebie tego activity
        wpisanyAdresUrl = (EditText) findViewById(R.id.editTextF);
        Button btnDalejNazwa = (Button) findViewById(R.id.btnDalej);



      //  pozniej intrefejs dla przycisku ktory nasluchuje klikniec

                btnDalejNazwa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // tutaj odczytujemy w momencie kiikniecia aktualny stan tekstu wpisanego w kontrolke EditText
                wpisanyTekst = wpisanyAdresUrl.getText().toString();
                Log.d(TAG, wpisanyTekst);

                // przechodzimy do drugiej aktywnoci
                Intent i = new Intent(FirstActivity.this, DrugieActivity.class);

                // pierwszy klucz, druga wartosc
                i.putExtra("dana_kod", wpisanyTekst);
                startActivity(i);
            }
        });
        }



    @Override
    protected void onRestart() {
        super.onRestart();
        Log.d(TAG, "FirstActivity: onRestart()");
        // aktywnosc w trakcie powracania na ekran
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d(TAG, "FirstActivity: onStart()");
        // aktywnosc przed ukazaniem sie na ekranie
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "FirstActivity: onResume()");
        // aktywnosc jest juz na ekranie
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d(TAG, "FirstActivity: onPause()");
        //aktywnosc przed przyslonieciem przez inna aktywnosc
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d(TAG, "FirstActivity: onStop()");
        // aktywnosc nie jest juz widoczna
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "FirstActivity: onDestroy()");
        // aktywnosc przed usunieciem z pamieci
    }
}
