package nowak.dariusz.aplikacjakurs;

import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.View;
import android.widget.Adapter;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CountryActivity extends AppCompatActivity {

    final String TAG = "kru";


    TextView tv;

    ArrayAdapter<String> adapter;


    String[] countrysTab = {"Polska", "Sosnowiec", "Kambodża", "Watykan", "Włochy",
            "Portugalia", "Hiszpania", "Andora", "Francja", "Lichsztenstain",
            "Austria", "Szwajcaira", "Luksemburg", "Monaco", "San Marino",
            "Słowacja", "Czychy", "Węgry", "Słowenia", "Rumunia"};

    ArrayList<String> countrys = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_country);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

//        Log.d(TAG, "onCreate");
//        ButterKnife.bind(this);
//        countrys.addAll(Arrays.asList(countrysTab));
//        adapter = new ArrayAdapter<String>(this, R.layout.row, countrys);

        tv = (TextView) findViewById(R.id.tv);

        String country = "";

        for (String s : countrysTab) {
            country += s + "<br>";
        }

        tv.setText(fromHTML(country));


        //listView.setAdapter(adapter);

    }

    @SuppressWarnings("deprecation")
    public static Spanned fromHTML(String HTML) {
        Spanned result;
        if(android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N){
            result = Html.fromHtml(HTML, Html.FROM_HTML_MODE_LEGACY);
        } else {
            result = Html.fromHtml(HTML);
        }
        return result;
    }


}
