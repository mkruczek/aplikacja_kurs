package nowak.dariusz.aplikacjakurs;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.Toast;
import android.widget.ToggleButton;

import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RejestracjaActivity extends AppCompatActivity {

    @BindView(R.id.userNameWrapper)
    TextInputLayout userNameWrapper;

    @BindView(R.id.userSurnameWrapper)
    TextInputLayout userSurnameWrapper;

    @BindView(R.id.numberPhoneWrapper)
    TextInputLayout numberPhoneWrapper;

    @BindView(R.id.nameEditText)
    EditText nameEditText;

    @BindView(R.id.surnameEditText)
    EditText surnameEditText;

    @BindView(R.id.postCodeEditText)
    EditText postCodeEditText;

    @BindView(R.id.regCheckBox)
    CheckBox regCheckBox;

    @BindView(R.id.spamToggleButton)
    ToggleButton spamToggleButton;

    @BindView(R.id.numberPhoneEditText)
    EditText numberPhoneEditText;

    @BindView(R.id.autoCompleteTextView)
    AutoCompleteTextView autoCompleteTextView;

    @BindView(R.id.regButton)
    Button regButton;

    String myVar;


    public void registry(View v) {

        if (nameEditText.getText().toString().length() == 0) {
//            Toast toast = Toast.makeText(getApplicationContext(), "PODAJ IMIĘ!", Toast.LENGTH_SHORT);
//            toast.show();
            userNameWrapper.setError("PODAJ IMIĘ!!");
            return;
        } else {
            userNameWrapper.setErrorEnabled(false);
        }
        if (surnameEditText.getText().toString().length() == 0) {
            Toast toast = Toast.makeText(getApplicationContext(), "PODAJ NAZWISKO!", Toast.LENGTH_SHORT);
            toast.show();
        }
        if (!regCheckBox.isChecked()) {
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
            alertDialogBuilder.setTitle("REGULAMIN");

            alertDialogBuilder
                    .setMessage("Zaakceptuj Regulamin.")
                    .setCancelable(false)
                    .setPositiveButton("Akceptuję", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            regCheckBox.setPressed(true);
                        }
                    })
                    .setNegativeButton("Spadaj", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            RejestracjaActivity.this.finish();
                            dialog.cancel();
                        }
                    });
            AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();
        }
        if (numberPhoneEditText.getText().toString().length() != 9) {
            numberPhoneWrapper.setError("PODJA PRAWIDOŁWY TELEFON!!");
        } else {
            numberPhoneWrapper.setErrorEnabled(false);
        }

        Toast toastREG = Toast.makeText(getApplicationContext(), "ZAREJESTROWANY", Toast.LENGTH_SHORT);
        toastREG.show();

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rejestracja);
        ButterKnife.bind(this);

        postCodeEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() == 2) {
                    String str = postCodeEditText.getText().toString();
                    str += "-";
                    postCodeEditText.setText(str);
                    postCodeEditText.setSelection(str.length());
                }
            }
        });

        final String[] tabCountrys = getResources().getStringArray(R.array.countrys);
        List<String> listCountrys = Arrays.asList(tabCountrys);
        final ArrayAdapter<String> adapterListCountrys = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, listCountrys);
        autoCompleteTextView.setAdapter(adapterListCountrys);

        autoCompleteTextView.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if(!b) {

                    String str = autoCompleteTextView.getText().toString();

                    ListAdapter listAdapter = autoCompleteTextView.getAdapter();
                    for(int i = 0; i < listAdapter .getCount(); i++) {
                        String temp = listAdapter .getItem(i).toString();
                        if(str.compareTo(temp ) == 0) {
                            return;
                        }
                    }

                    autoCompleteTextView.setText("");

                }
            }
        });

        regButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(RejestracjaActivity.this, MyAccountActivity.class);
//                i.putExtra("firstName", nameEditText.getText().toString());
//                i.putExtra("surName", surnameEditText.getText().toString());

                User newMan = new User(nameEditText.getText().toString(), surnameEditText.getText().toString(), spamToggleButton.isChecked());
                i.putExtra("USER", newMan);

                startActivity(i);
            }
        });


    }


}
