package nowak.dariusz.aplikacjakurs;

import java.text.DecimalFormat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class calculatorActivity extends AppCompatActivity {


    @BindView(R.id.score)
    TextView score;

    @BindView(R.id.szerokosc)
    EditText szerokosc;

    @BindView(R.id.wysokosc)
    EditText wysokosc;

    @BindView(R.id.rozmiar)
    EditText rozmiar;

    @BindView(R.id.imageButton)
    ImageButton imageButton;

    DecimalFormat df;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calculator);

        ButterKnife.bind(this);

    }

    public void klik(View v) {

        double sze = Integer.parseInt(szerokosc.getText().toString());
        double wys = Integer.parseInt(wysokosc.getText().toString());
        double roz = Double.parseDouble(rozmiar.getText().toString());

        double wynik = (Math.sqrt(Math.pow(sze,2) + Math.pow(wys,2)))/roz;

        df = new DecimalFormat();
        df.setMaximumFractionDigits(2);

        score.setText(String.valueOf(df.format(wynik)));

        Animation rotation = AnimationUtils.loadAnimation(this, R.anim.rotate);
        imageButton.setRotation(0);
        imageButton.startAnimation(rotation);


    }
}
