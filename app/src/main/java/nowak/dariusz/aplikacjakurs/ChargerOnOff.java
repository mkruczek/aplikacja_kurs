package nowak.dariusz.aplikacjakurs;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.util.Log;
import android.widget.Toast;

/**
 * Created by RENT on 2017-06-07.
 */

public class ChargerOnOff extends BroadcastReceiver {



    @Override
    public void onReceive(Context context, Intent intent) {

        if (intent.getAction().equals("android.intent.action.ACTION_POWER_CONNECTED")) {
            Toast.makeText(context, "Telefon został podłączony do ładowarki", Toast.LENGTH_LONG).show();
        }

        if (intent.getAction().equals("android.intent.action.ACTION_POWER_DISCONNECTED")) {
            Toast.makeText(context, "Telefon został odłączony od ładowarki", Toast.LENGTH_LONG).show();
        }

    }
}
