package nowak.dariusz.aplikacjakurs;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

    }

    public void countryActivity(View v){
        Intent i = new Intent(MainActivity.this, CountryActivity.class);
        MainActivity.this.startActivity(i);
    }

    public void fotoActivity(View v){
        Intent i = new Intent(MainActivity.this, FotoActivity.class);
        MainActivity.this.startActivity(i);
    }

    public void smsActivity(View v){
        Intent i = new Intent(MainActivity.this, SMSActivity.class);
        MainActivity.this.startActivity(i);
    }

    public void gameActivity(View v){
        Intent i = new Intent(MainActivity.this, GameActivity.class);
        MainActivity.this.startActivity(i);
    }

}
