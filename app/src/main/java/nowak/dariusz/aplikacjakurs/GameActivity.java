package nowak.dariusz.aplikacjakurs;

import android.content.DialogInterface;
import android.graphics.Color;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;

public class GameActivity extends AppCompatActivity {


    @BindView(R.id.scoreMe)
    TextView scoreMe;

    @BindView(R.id.scoreComputer)
    TextView scoreComputer;

    @BindView(R.id.roundNumber)
    TextView roundNumber;

    @BindView(R.id.choseMeImg)
    ImageView choseMeImg;

    @BindView(R.id.choseComputerIMG)
    ImageView choseComputerIMG;

    @BindView(R.id.rock)
    Button rock;

    @BindView(R.id.scissors)
    Button scissors;

    @BindView(R.id.paper)
    Button paper;

    static LinearLayout mainLayout;

    private static int myPointCounter;
    private static int computerPointCounter;
    private static int roundCounter;
    private static int duration;
    private static int computerIMG;
    private static int myIMG;
    private static boolean lastGame = true;
    private String whoWin;
    private static boolean useUndo;

    final int[] figures = {R.drawable.rock,
            R.drawable.paper,
            R.drawable.scissors};


    public void setDuration(int duration) {
        this.duration = duration;
    }

    public int getDuration() {
        return duration;
    }

    public void setDurationNumber(boolean value) {
        if (value) {
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
            alertDialogBuilder.setTitle("Papier Kamień Nożyce");
            alertDialogBuilder
                    .setMessage("WYBIERZ ILE RUND ROZEGRAĆ")
                    .setCancelable(false)
                    .setPositiveButton("5", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            setDuration(5);
                            dialog.cancel();
                        }
                    })
                    .setNegativeButton("9", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            setDuration(9);
                            dialog.cancel();
                        }
                    });
            AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();
        }
    }


    private int randomFigure(int[] figures) {
        Random rd = new Random();
        return figures[rd.nextInt(figures.length)];
    }

    private void end(int x) {
        if (x == getDuration()) {
            String setMessage = "";
            if (myPointCounter > computerPointCounter) {
                setMessage = "WYGRANA!!";
            } else if (myPointCounter < computerPointCounter) {
                setMessage = "WTOPA!!";
            } else {
                setMessage = "REMISIK!!";
            }

            ImageView image = new ImageView(this);
            image.setImageResource(R.drawable.whonaplayagain);

            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
            alertDialogBuilder.setTitle("Koniec");
            alertDialogBuilder
                    .setMessage(setMessage)
                    .setCancelable(false)
                    .setPositiveButton("TAK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            reset();
                        }
                    })
                    .setNegativeButton("Spadaj", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            GameActivity.this.finish();
                            dialog.cancel();
                        }
                    }).setView(image);
            AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();
        }
    }

    private void reset() {
        roundCounter = 0;
        myPointCounter = 0;
        computerPointCounter = 0;

        scoreMe.setText(0 + "");
        scoreComputer.setText(0 + "");
        roundNumber.setText(0 + "");

        useUndo = false;

        setDurationNumber(true);
    }



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);
        ButterKnife.bind(this);

        useUndo = false;

        mainLayout = (LinearLayout) findViewById(R.id.mainLayout);

        setDurationNumber(lastGame);


        paper.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                choseMeImg.setImageResource(R.drawable.paper);
                whoWin = "";
                myIMG = R.drawable.paper;

                int fig = randomFigure(figures);
                choseComputerIMG.setImageResource(fig);
                roundCounter++;
                roundNumber.setText(roundCounter + "");

                computerIMG = fig;

                if (fig == 2130837610) {
                    myPointCounter++;
                    scoreMe.setText(myPointCounter + "");
                    whoWin = "me";
                } else if (fig == 2130837611) {
                    computerPointCounter++;
                    scoreComputer.setText(computerPointCounter + "");
                    whoWin = "cpu";
                }
                Log.d(TAG, whoWin);
                snackBar(whoWin, scoreMe, scoreComputer, roundNumber);
                end(roundCounter);

            }
        });

        rock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                choseMeImg.setImageResource(R.drawable.rock);
                whoWin = "";
                myIMG = R.drawable.rock;

                int fig = randomFigure(figures);
                choseComputerIMG.setImageResource(fig);
                roundCounter++;
                roundNumber.setText(roundCounter + "");

                computerIMG = fig;

                if (fig == 2130837611) {
                    myPointCounter++;
                    scoreMe.setText(myPointCounter + "");
                    whoWin = "me";
                } else if (fig == 2130837609) {
                    computerPointCounter++;
                    scoreComputer.setText(computerPointCounter + "");
                    whoWin = "cpu";
                }
                Log.d(TAG, ""+whoWin);
                snackBar(whoWin, scoreMe, scoreComputer, roundNumber);
                end(roundCounter);
            }
        });

        scissors.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                choseMeImg.setImageResource(R.drawable.scissors);
                whoWin = "";
                myIMG = R.drawable.scissors;

                int fig = randomFigure(figures);
                choseComputerIMG.setImageResource(fig);
                roundCounter++;
                roundNumber.setText(roundCounter + "");

                computerIMG = fig;

                if (fig == 2130837609) {
                    myPointCounter++;
                    scoreMe.setText(myPointCounter + "");
                    whoWin = "me";
                } else if (fig == 2130837610) {
                    computerPointCounter++;
                    scoreComputer.setText(computerPointCounter + "");
                    whoWin = "cpu";
                }
                Log.d(TAG, whoWin);
                snackBar(whoWin, scoreMe, scoreComputer, roundNumber);
                end(roundCounter);
            }
        });

        paper.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {

                myPointCounter = 4;
                scoreMe.setText(myPointCounter + "");

                return false;
            }
        });

    }

    final String TAG = "kru";
    final String SAVED_CLICK_myPoint = "myPoint";
    final String SAVED_CLICK_computerPointCounter = "computerPoint";
    final String SAVED_CLICK_roundCounter = "roundCounter";
    final String SAVED_CLICK_lastGame = "lastGame";
    final String SAVED_myIMAGE = "myIMG";
    final String SAVED_computerIMG = "computerIMG";

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(SAVED_CLICK_myPoint, myPointCounter);
        outState.putInt(SAVED_CLICK_computerPointCounter, computerPointCounter);
        outState.putInt(SAVED_CLICK_roundCounter, roundCounter);
        outState.putBoolean(SAVED_CLICK_lastGame, false);
        outState.putInt(SAVED_myIMAGE, myIMG);
        outState.putInt(SAVED_computerIMG, computerIMG);

    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        myPointCounter = savedInstanceState.getInt(SAVED_CLICK_myPoint);
        scoreMe.setText(String.valueOf(myPointCounter));

        computerPointCounter = savedInstanceState.getInt(SAVED_CLICK_computerPointCounter);
        scoreComputer.setText(String.valueOf(computerPointCounter));

        roundCounter = savedInstanceState.getInt(SAVED_CLICK_roundCounter);
        roundNumber.setText(String.valueOf(roundCounter));

        choseMeImg.setImageResource(savedInstanceState.getInt(SAVED_myIMAGE));
        choseComputerIMG.setImageResource(savedInstanceState.getInt(SAVED_computerIMG));

        lastGame = savedInstanceState.getBoolean(SAVED_CLICK_lastGame);


    }

    public void snackBar(String str, final TextView myPoint, final TextView cpuPoint, final TextView roundNumber){

        LinearLayout layout = mainLayout;



        if(str.equals("me")){

            Snackbar sb = Snackbar.make(layout, "Wygrałeś Runde", Snackbar.LENGTH_LONG)
                    .setActionTextColor(Color.GREEN)
                    .setAction("UNDO", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            myPointCounter -= 1;
                            myPoint.setText(myPointCounter+"");
                            roundCounter -= 1;
                            roundNumber.setText(roundCounter+"");
                        }
                    });
            View viewSB = sb.getView();
            TextView tvSB = (TextView) viewSB.findViewById(android.support.design.R.id.snackbar_text);
            tvSB.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.green ));
            sb.show();

        } else if(str.equals("cpu")){
            Snackbar sb = Snackbar.make(layout, "Komputer Wygrał", Snackbar.LENGTH_LONG)
                    .setActionTextColor(Color.RED)
                    .setAction("UNDO", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            computerPointCounter -= 1;
                            cpuPoint.setText(computerPointCounter+"");
                            roundCounter -= 1;
                            roundNumber.setText(roundCounter+"");
                        }
                    });
            View viewSB = sb.getView();
            TextView tvSB = (TextView) viewSB.findViewById(android.support.design.R.id.snackbar_text);
            tvSB.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.red ));
            sb.show();
        } else {
            Snackbar.make(layout, "REMISIK", Snackbar.LENGTH_LONG)
                    .setAction("UNDO", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            roundCounter -= 1;
                            roundNumber.setText(roundCounter+"");
                        }
                    }).show();
        }
    }
}
