package nowak.dariusz.aplikacjakurs;

import android.graphics.Bitmap;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import pl.sdacademy.secretphoto.PhotoSecretReturner;

public class FotoActivity extends AppCompatActivity {

    final String TAG = "kru";

    @BindView(R.id.imageView2)
    ImageView kontrolkaZdjecia;



    @OnClick(R.id.fab)
    public void wczytajFoto(){

        String[] source = {"https://pbs.twimg.com/profile_images/1335749456/foka.jpg",
                "http://dinoanimals.pl/wp-content/uploads/2014/01/Rottweiler-14.jpg",
                "http://bi.gazeta.pl/im/ae/62/c4/z12870318Q.jpg?preview=1"};

        PhotoSecretReturner psr = new PhotoSecretReturner();

        String imageUri = psr.photo(source);
        Log.d(TAG,imageUri);
        ImageLoader.getInstance().displayImage(imageUri,kontrolkaZdjecia);


    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_foto);

        Log.d(TAG, "START");


        ButterKnife.bind(this);

        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(this).build();
        ImageLoader.getInstance().init(config);

//        https://pbs.twimg.com/profile_images/830455748111036416/ZFMmVyYR.jpg

    }
}
