package nowak.dariusz.aplikacjakurs;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.Toast;

import java.util.Timer;
import java.util.TimerTask;

import static android.media.MediaPlayer.*;

/**
 * Created by RENT on 2017-06-08.
 */

public class MyService extends Service {
    final Handler handler = new Handler();
    static int counter = 1;


    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    public int onStartCommand(Intent intent, int flags, int startId) {
        Context context = getApplicationContext();
        doToast(context);
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        Toast.makeText(this, "service stop", Toast.LENGTH_SHORT).show();
        handler.removeCallbacksAndMessages(null);


    }


    public void doToast(final Context context) {
        Toast.makeText(context, "Tost nr.: 1", Toast.LENGTH_SHORT).show();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                String str = "Tost nr.: " + ++counter;
                Toast.makeText(context, str, Toast.LENGTH_SHORT).show();
                handler.postDelayed(this, 5000);
            }
        }, 5000);

    }

}
