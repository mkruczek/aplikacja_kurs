package pl.sdacademy.secretphoto;

import java.util.Arrays;
import java.util.List;
import java.util.Random;

/**
 * Created by RENT on 2017-05-25.
 */

public class PhotoSecretReturner {

    static int lastrandom = 0;

    String[] source = {"https://pbs.twimg.com/profile_images/1335749456/foka.jpg",
                        "http://dinoanimals.pl/wp-content/uploads/2014/01/Rottweiler-14.jpg",
                        "http://bi.gazeta.pl/im/ae/62/c4/z12870318Q.jpg?preview=1"};

    public  String photo(String[] source) {

        Random rd = new Random();
        int actuallyRd = rd.nextInt(source.length);

        while (actuallyRd == lastrandom) {
            actuallyRd = rd.nextInt(source.length);
        }

        lastrandom = actuallyRd;

        return source[actuallyRd];
    }
}
